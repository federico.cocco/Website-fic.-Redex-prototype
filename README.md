# Description
This is a fictional website to practise with html/css and general design 
practises.

A viewable version of this prototype is currently hosted at:

	http://titan.dcs.bbk.ac.uk/~fcocco01/infma/
	
## Other info

For: Birkbeck College
Year: 2014/2015
Author: Federico Cocco